import React from 'react';
import { Link } from "react-router-dom";
import "./Homepage.css";
import theme from "../../images/logo 1.png";
function Homepage() {
  return (
    <div>


      <nav>
        <label className="theme"><img src={theme} /></label>
        <ul>
          <li><a className="active "style={{ textDecoration: "none"}} href="index.html">Home</a></li>
          <Link to={"/register"} id="insde2" style={{ textDecoration: "none", color: 'black', padding: ' 7px 13px', height: '50px', fontSize: '18px', borderRadius: '5px', transition: '0.5s ease' }}>Projects</Link>
          <Link to={"/register"} id="insde2" style={{ textDecoration: "none", color: 'black', padding: ' 7px 13px', height: '50px', fontSize: '18px', borderRadius: '5px', transition: '0.5s ease' }}>Shares</Link>
          <Link to={"/register"} id="insde2" style={{ textDecoration: "none", color: 'black', padding: ' 7px 13px', height: '50px', fontSize: '18px', borderRadius: '5px', transition: '0.5s ease' }}>Join Us</Link>
          <Link to={"/login"} id="insde2" style={{ textDecoration: "none", color: 'black', padding: ' 7px 13px', height: '50px', fontSize: '18px', borderRadius: '5px', transition: '0.5s ease' }}>log in</Link>


        </ul>
      </nav>


      <div className="teleza">
        <input type="king" id="i1" name="picha" defaultChecked />
        <input type="king" id="i2" name="picha" />
        <input type="king" id="i3" name="picha" />

        <div className="pichamtelezo" id="one">
          <img src="images/Assets/homepage carousel/invest.jpeg" alt="" />

          <label className="prev" htmlFor="i5"><span>&#x2039;</span></label>
          <label className="next" htmlFor="i2"><span>&#x203a;</span></label>
        </div>

        <div className="pichamtelezo" id="two">
          <img src="images/Assets/homepage carousel/invest.jpeg" alt="" />

          <label className="prev" htmlFor="i1"><span>&#x2039;</span></label>
          <label className="next" htmlFor="i3"><span>&#x203a;</span></label>
        </div>

        {/* <div className="pichamtelezo" id="three">
          <img src="" alt="" />

          <label className="prev" htmlFor="i2"><span>&#x2039;</span></label>
          <label className="next" htmlFor="i4"><span>&#x203a;</span></label>
        </div> */}
      </div>


      <h6 className="opaque">CURRENT OPPORTUNITIES</h6>
      <section className="current-opp">
        <div className="opp-row">
          <div className="opportunities">
            <img src="images/Assets/Farm.png" alt="" />
            <h3>Bora Estate</h3>
            <p>
              Lorem ipsum dolor sit,adipisicingelit.
              Illo, quaerat?
            </p>
            <button className="invest">
              <a href=""> Invest </a>
            </button>
          </div>

          <div className="opportunities">
            <img src="images/Assets/Farm.png" alt="" />
            <h3>Tunda Estate</h3>
            <p>
              Lorem ipsum dolor sit,adipisicingelit.
              Illo.
            </p>
            <button className="invest">
              <a href=""> Invest </a>
            </button>
          </div>

          <div className="opportunities">
            <img src="images/Assets/Farm.png" alt="" />
            <h3>Majani Tea</h3>
            <p>
              Lorem ipsum dolor sit,amet consectetur
            </p>
            <button className="invest" ><a href="new shares.html"> Invest </a></button>
          </div>
        </div>
      </section>


      <div className="services-container">
        <h1>OUR SERVICES</h1>
        <div className="serve-row">
          <div className="services-images">
            <img src="images/Assets/investment 4.jpg" alt="" />
            <h2>Invest</h2>
          </div>

          <div className="services-image">
            <img src="images/Assets/icons/61484_chart_graph_stock_icon (1).png" alt="" />
            <h2>Shares</h2>
          </div>

          <div className="services-image">
            <img src="images/Assets/icons/Multicolor.png" alt="" />
            <h2>Raise Capital</h2>
          </div>
        </div>
      </div>




      <div className="phil-container">
        <h1>OUR PHILOSOPHY</h1>
        <div className="phil">
          <div className="phil-img">
            <img src="images/Assets/img23.png" alt="" />
          </div>
          <p>Amal Capital provides a one stop shop htmlfor proposal
            writing, Capital Raising, Project
            and project Management, Investor engagement and Services to
            organizations and corprate.Amal Capital provides
            a one stop shop htmlfor proposal writing
            Capital Raising, Project and project
            Management, Investor engagement and Services to organizations and corprate.Amal Capital provides a one stop shop htmlfor proposal
            writing, Capital Raising, Project
            and project Management, Investor engagement and Services to
            organizations and corprate</p>
        </div>
      </div>


      <div className="partners-container">
        <h1>Companies we have worked with</h1>
        <div className="part-row">
          <div className="part-images">
            <img className="part-image" src="images/Assets/icons/3225181_app_logo_media_popular_social_icon.png" alt="" />
            <h2>Hass Consult</h2>
          </div>

          <div className="part-images">
            <img className="part-image" src="images/Assets/icons/1156735_finance_logo_maestro_payment_icon.png" alt="" />
            <h2>Western Union</h2>
          </div>

          <div className="part-images">
            <img className="part-image" src="images/Assets/icons/1156749_finance_logo_payment_western union_icon.png" alt="" />
            <h2>Mastercard</h2>
          </div>

          <div className="part-images">
            <img className="part-image" src="images/Assets/icons/_x33_2-teamviewer.png" alt="" />
            <h2>TeamViewer</h2>
          </div>
        </div>
      </div>



      <footer className="footer">
        <div className="footcontainer">
          <div className="footer-row">
            <div className="footer-col">
              <h4>About Us</h4>
              <p>
                Amal Capital provides a one stop shop htmlfor proposalwriting, Capital Raising, Project analysis and project Management, Investor engagement and Services to organizations and corprate
              </p>
            </div>

            <div className="footer-col">
              <h4>What we do.</h4>
              <ul>
                <li><a href="#">Credit</a></li>
                <li><a href="#">Investments</a></li>
                <li><a href="#">Public Equity</a></li>
                <li><a href="#">Real Estate</a></li>
                <li><a href="#">Finance</a></li>
              </ul>
            </div>


            <div className="footer-col">
              <h4>Quick Links</h4>
              <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">The Company</a></li>
                <li><a href="#">Our Values</a></li>
                <li><a href="#">Our policy</a></li>
                <li><a href="#">Direct Investments</a></li>
              </ul>
            </div>

            <div className="footer-col">
              <h4>Contact Details</h4>
              <ul>
                <li>Amal capital FCB Lenana Rd,Nairobi Kenya</li>
                <li>Telephone</li>
                <li>+254 705639782</li>
                <li>+254 757240816</li>
                <li>+254 206718636</li>
              </ul>
            </div>

          </div>
        </div>
      </footer>



    </div>
  );
}

export default Homepage;