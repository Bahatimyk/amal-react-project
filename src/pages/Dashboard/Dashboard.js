import React from 'react';
import Sidebar from "../../components/sidebar/Sidebar";
import ProfileNav from '../../components/card/ProfileNav';
import "./Dashboard.css";
import 'bootstrap/dist/css/bootstrap.css';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label } from 'recharts';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import { PieChart, Pie, Sector, Cell, } from 'recharts';
// import Progress from 'react-circle-progress-bar'

// import 'react-circular-progressbar/dist/styles.css';    
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import { CircularProgressbarWithChildren } from 'react-circular-progressbar';
  
  


function Dashboard() {
 

  const data = [
    {
      name: 'Jan',
      Amount: 2400,
    },
    {
      name: 'Feb',
      Amount: 1398,
    },
    {
      name: 'Mar',
      Amount: 9800,
    },
    {
      name: 'Apr',
      Amount: 3908,
    },
    {
      name: 'May',
      Amount: 4800,
    },
    {
      name: 'Jun',
      Amount:  3800,
    },
    {
      name: 'July',
      Amount: 4300,
    },
    {
      name: 'Aug',
      Amount:5000,
    },
    {
      name: 'Sep',
      Amount: 4500,
    },
    {
      name: 'Oct',
      Amount:3900,
    },
    {
      name: 'Nov',
      Amount: 4000,
    },

    {
      name: 'Dec',
      Amount: 4300,
    },
  ];

  const percentage = 7;
  const no = 66;
   
  
  const rick= 25;
  const mono=66;
  
  const morty= 80;
  const eddy= 80;
  const aku= 70;
  
  
//    slick
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3
      };
     
   


  return (
    <div>
<Sidebar/>
<ProfileNav/>
    
        
          
        <section >
       
        <div className="container">
            <div className="mpemba">
                <div className="mtongwe"style={{borderRadius:"10px"}}>
                  

                  {/* card a */}

                      

                    <h4 className="card-heading">number of projects</h4>
                    {/* <div style={{ width: "100", height: "100" }}>
                          
                          <CircularProgressbar value={80}  />
                                 </div> */}
< div style={{width:"80%", height:"65%",marginLeft: "10%", }} >
<CircularProgressbar
  value={percentage}
  text={'7'}
  
  styles={buildStyles({
    // Rotation of path and trail, in number of turns (0-1)
    rotation: 0.25,

    // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
    strokeLinecap: 'butt',

    // Text size
    textSize: '16px',textAlign:'center',
    

    // How long animation takes to go from one percentage to another, in seconds
    pathTransitionDuration: 0.5,

    // Can specify path transition in more detail, or remove it entirely
    // pathTransition: 'none',

    // Colors
    
    pathColor: `rgba(62, 152, 199, ${no / 100})`,
    textColor: 'black',
    trailColor: '#d6d6d6',
    backgroundColor: '#3e98c7',
  })}


  
/>

 </div>                         
                          
                         
                                 
                    <div className="card-body">

                        <div className="join" style={{overflow:"hidden"}}>
                            <h6 style={{display: "flex"}}>completed <span style={{paddingLeft: "2px"}}> projects:</span></h6>
                            <input className="boarderless" type="text" placeholder="2"/>
                        </div>
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>monthly <span style={{paddingLeft: "2px"}}> Target:</span></h6>
                            <input className="boarderless" type="text" placeholder="1"/>
                        </div>
                      </div>
                </div>

              {/* card b */}
                <div className="mtongwe"style={{borderRadius:"10px"}}>

                        <select className="boarderlesss">
                            <option>2021</option>
                            <option>2020</option>
                            <option>2019</option>
                        </select>
                         
                  
                        {/* <div style={{ width: "100", height: "100" }}>
                          
                          <CircularProgressbar value={80}  />
                                 </div> */}
 < div style={{width:"80%", height:"70%",marginLeft: "10%", }} >                 
<CircularProgressbar
  value={rick}
  text={25}
  styles={buildStyles({
    // Rotation of path and trail, in number of turns (0-1)
    rotation: 0.25,

    // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
    strokeLinecap: 'butt',

    // Text size
    textSize: '16px',textAlign:'center',
    // width: "100", height: "100" ,

    // How long animation takes to go from one percentage to another, in seconds
    pathTransitionDuration: 0.5,

    // Can specify path transition in more detail, or remove it entirely
    // pathTransition: 'none',

    // Colors
    pathColor: `rgba(62, 152, 199, ${mono/ 100})`,
    textColor: 'black',
    trailColor: '#d6d6d6',
    backgroundColor: '#3e98c7',
  })}
/>
</div>
                    
                    <div className="card-body">
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>completed <span style={{paddingLeft: "2px"}}> projects:</span></h6>
                            <input className="boarderless" type="text" placeholder="2"/>
                        </div>
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>monthly <span style={{paddingLeft: "2px"}}> Target:</span></h6>
                            <input className="boarderless" type="text" placeholder="4"/>
                        </div>
                    </div>
                </div>

         {/* card c       */}
           <div className="mtongwe"style={{borderRadius:"10px"}}>
                    <select className="boarderlesss">
                        <option>2021</option>
                        <option>2020</option>
                        <option>2019</option>
                    </select>
                   
                    {/* <div style={{ width: "100", height: "100" }}>
                          <CircularProgressbar value={50} />
                                 </div> */}
  < div style={{width:"80%", height:"70%",marginLeft: "10%", }} >                     
<CircularProgressbar
  value={morty}
  text={`${morty}%`}
  styles={buildStyles({
    // Rotation of path and trail, in number of turns (0-1)
    rotation: 0.25,

    // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
    strokeLinecap: 'butt',

    // Text size
    textSize: '16px',
    width: "100", height: "100" ,

    // How long animation takes to go from one percentage to another, in seconds
    pathTransitionDuration: 0.5,

    // Can specify path transition in more detail, or remove it entirely
    // pathTransition: 'none',

    // Colors
    pathColor: `rgba(62, 152, 199, ${morty / 100})`,
    textColor: 'black',
    trailColor: '#d6d6d6',
    backgroundColor: '#3e98c7',
  })}
/> 
  </div>                  
                    <div className="card-body">
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>completed <span style={{paddingLeft: "2px"}}> projects:</span></h6>
                            <input className="boarderless" type="text" placeholder="2"/>
                        </div>
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>monthly <span style={{paddingLeft: "2px"}}> Target:</span></h6>
                            <input className="boarderless" type="text" placeholder="3"/>
                        </div>
                    </div>
                </div>
                
{/* card d */}
                 <div className="mtongwe"style={{borderRadius:"10px"}}>
                    <div className="join">
                        <h4 className="card-heading">shares</h4>
                        <select className="boarderlesss">
                            <option>2021</option>
                            <option>2020</option>
                            <option>2019</option>
                        </select>
                    </div>
                  
                        
                    {/* <div style={{ width: "100", height: "100"  }}>
                          <CircularProgressbar value={25} />
                                 </div> */}
  < div style={{width:"80%", height:"65%",marginLeft: "10%", }} >                     
<CircularProgressbar
  value={eddy}
  text={`${eddy}%`}
  styles={buildStyles({
    // Rotation of path and trail, in number of turns (0-1)
    rotation: 0.25,

    // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
    strokeLinecap: 'butt',

    // Text size
    textSize: '16px',
    width: "100", height: "100" ,

    // How long animation takes to go from one percentage to another, in seconds
    pathTransitionDuration: 0.5,

    // Can specify path transition in more detail, or remove it entirely
    // pathTransition: 'none',

    // Colors
    pathColor: `rgba(62, 152, 199, ${eddy / 100})`,
    textColor: 'black',
    trailColor: '#d6d6d6',
    backgroundColor: '#3e98c7',
  })}
/>  
 </div>                 
                    <div className="card-body">
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>completed <span style={{paddingLeft: "2px"}}> projects:</span></h6>
                            <input className="boarderless" type="text" placeholder="2"/>
                        </div>
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>monthly <span style={{paddingLeft: "2px"}}> Target:</span></h6>
                            <input className="boarderless" type="text" placeholder="3"/>
                        </div> 
                  </div></div>
                   
                  <div className="mtongwe"style={{borderRadius:"10px"}}>
                    <div className="join">
                        <h4 className="card-heading">shares</h4>
                        <select className="boarderlesss">
                            <option>2021</option>
                            <option>2020</option>
                            <option>2019</option>
                        </select>
                    </div>
                  
{/*                         
                    <div style={{ width: "200", height: "200"  }}>
                          <CircularProgressbar value={90} />
                                 </div> */}
   < div style={{width:"80%", height:"65%",marginLeft: "10%", }} >                       
<CircularProgressbar
  value={aku}
  text={`${aku}%`}
  styles={buildStyles({
    // Rotation of path and trail, in number of turns (0-1)
    rotation: 0.25,

    // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
    strokeLinecap: 'butt',

    // Text size
    textSize: '16px',
    width: "100", height: "100" ,

    // How long animation takes to go from one percentage to another, in seconds
    pathTransitionDuration: 0.5,

    // Can specify path transition in more detail, or remove it entirely
    // pathTransition: 'none',

    // Colors
    pathColor: `rgba(62, 152, 199, ${aku / 100})`,
    textColor: 'black',
    trailColor: '#d6d6d6',
    backgroundColor: '#3e98c7',
  })}
/>
</div>
                  
                    <div className="card-body">
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>completed <span style={{paddingLeft: "2px"}}> projects:</span></h6>
                            <input className="boarderless" type="text" placeholder="2"/>
                        </div>
                        <div className="join" style={{overflow: "hidden"}}>
                            <h6 style={{display: "flex"}}>monthly <span style={{paddingLeft: "2px"}}> Target:</span></h6>
                            <input className="boarderless" type="text" placeholder="3"/>
                        </div> 
                  </div>
            
               </div>
               </div>
              
            
            

                
            {/* line graph */}

            <div className="middle-cards padd-15">
                <div className="line-graph ">
                {/* <h1 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"bold"}}>Money in USD$</h1> */}
                <ResponsiveContainer width="100%" height="100%">
                 
        <LineChart
          width={500}
          height={300}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="Amount" stroke="#8884d8" activeDot={{ r: 8 }} />
          
        </LineChart>
      </ResponsiveContainer>
    
  

                </div>
                
                

                {/* progressive chart */}
                <div className="progress-div padd-15" >
                    <div className="padd-15">
                        <p>Sawa Estate</p>
                        <div className="progress" style={{backgroundColor: "rgb(245, 173, 173)", width:"100%",borderRadius: "10pX"}} >
                            <div className="prog" style={{backgroundColor: "red; width: 76%"}}> .</div>
                            {/* <!-- <div className="progress-in" style="width: 76%;background-color: red;"></div> --> */}
                            
                        </div>
                    </div><div className="padd-15">
                        <p>Beans Farm</p>
                        <div className="progress" style={{backgroundcolor: "rgb(199, 199, 243)", width:"100%,",borderRadius:  "10px"}} >
                            <div className="progress-in" style={{width: "56%",backgroundColor: "blue"}}> .</div>
                          
                        </div>
                    </div>
                    <div className="padd-15">
                        <p>Stima Power</p>
                        <div className="progress" style={{backgroundColor: "rgb(139, 247, 139)", width:"100%",borderRadius: "10px"}} >
                            <div className="progress-in" style={{width: "36%",backgrounColor: "green"}}>.</div>
                            
                        </div>
                    </div>

                    <div className="padd-15">
                        <p>Leaf holdings</p>
                        <div className="progress" style={{backgroundColor: "rgb(247, 247, 184)", width:"100%",borderRadius: "10px"}} >
                            <div className="progress-in" style={{width:"80%",backgroundColor: "yellow"}}>.</div>
                            
                        </div>
                    </div>
                    </div>
                    </div>

                    <Slider {...settings}>
      <div>
      <div className="boxed"style={{borderRadius: "10px"}}>
        <h1 style={{fontSize:"35px",textAlign:"center",fontFamily:"georgia",fontWeight:"lighter"}}>USD</h1>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        </div>
        
      </div>
      <div>
      <div className="boxed"style={{borderRadius: "10px"}}>
        <h1 style={{fontSize:"35px",textAlign:"center",fontFamily:"georgia",fontWeight:"light"}}>GBP</h1>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        <h2 style={{fontSize:"20px",textAlign:"justify",fontFamily:"georgia",fontWeight:"light"}} >Buy:111.78</h2>
        </div>
  
      </div>
      <div>
      <div className="boxed"style={{borderRadius: "10px"}}>
        <h1 style={{fontSize:"35px",textAlign:"center",fontFamily:"georgia",fontWeight:"light"}}>Euro</h1>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        </div>
      </div>
      <div>
      <div className="boxed"style={{borderRadius: "10px"}}>
        <h1 style={{fontSize:"35px",textAlign:"center",fontFamily:"georgia",fontWeight:"light"}}>JPY</h1>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        </div>
      </div>
      <div>
      <div className="boxed"style={{borderRadius: "10px"}}>
        <h1 style={{fontSize:"35px",textAlign:"center",fontFamily:"georgia",fontWeight:"light"}}>ZAR</h1>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        </div>
      </div>
      <div>
      <div className="boxed"style={{borderRadius: "10px"}}>
        <h1 style={{fontSize:"35px",textAlign:"center",fontFamily:"georgia",fontWeight:"light"}}>KSH</h1>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        <h2 style={{fontSize:"20px",textAlign:"left",fontFamily:"georgia",fontWeight:"light"}}>Buy:111.78</h2>
        </div>
      </div>
    </Slider>
  

                                   
            
               {/* // end progressive chart */}
          
  </div>  
 
  

</section>

</div>
    
  

  )
}

export default Dashboard;
