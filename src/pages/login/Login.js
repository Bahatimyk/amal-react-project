import React from 'react';
import './Login.css';
import logo from "../../images/logo 1.png";
import invest from "../../images/invest 6.jpg";
import or from "../../images/or4.jpg";
import { useState } from "react";
import {Link} from "react-router-dom";
import google from "../../images/google.png";
import { IoArrowForwardCircleOutline } from "react-icons/io5";
import din from "../../images/li.png";
import face from "../../images/fb.png";
import axios from 'axios';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import {Formik,  Form, ErrorMessage, Field} from 'formik';
import * as Yup from 'yup';
import {Formi} from './Formi'; 
import 'bootstrap/dist/css/bootstrap.min.css';


function Login() {

const validate =Yup.object({   
    email: Yup.string() 
    .email ("Must be valid")
    .required ("email is required"),
    password: Yup.string() 
    .min(6, "Must be valid") 
    .required ("pass is required"),
})




    const navigate = useNavigate();

        const [popup, setPopup]=useState(false);
      
        const togglePopup = () => {
            setPopup(!popup)
        };
      
        if(popup){
          document.body.classList.add('active')
        }
        else{
          document.body.classList.remove('active')
        }



        
        const [popup1, setPopup1]=useState(false);
      
        const togglePopup1 = () => {
            setPopup1(!popup1)
        };
      
        if(popup1){
          document.body.classList.add('active')
        }else{
            document.body.classList.remove('active') 
        }
       



        // event handlers

        const [ loginInput, setLogin] = useState ({
            email: '',
            password: '',
           
        });

const handleInput = (e) => {
    e.persist();
    setLogin({...loginInput, [e.target.name]: e.target.value});
}


const loginSubmit = (e) => {
    e.preventDefault();

    const data = {
        email: loginInput.email,
        password: loginInput.password,
    }

axios.post(`api/login`, data).then(res =>{

    
    if(res.status === 200)
    {

        localStorage.setItem("auth_token", res.data.token);
        localStorage.setItem("auth_userName", JSON.stringify(res.data.user));


        Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'You have successfully Logged into the System',
            showConfirmButton: true,
            
          })


        navigate('/Dashboard');

    }else
    {


        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Invalid Credentials!',
            footer: 'please check your credentials'
          })

        
    }
});

}




        const [ resetInput, resetLogin] = useState ({
            email: '',
            newPass: '',
            newConPass: '',
            errorlist: [],
        });

const handleNewInput = (e) => {
    e.persist();
    resetLogin({...resetInput, [e.target.name]: e.target.value});
}


const takeNewInput = (e) => {
    e.preventDefault();

    const data = {
        email: resetInput.email,
        newPass: resetInput.newPass,
        newConPass: resetInput.newConPass,
    }

axios.post(`api/login`, data).then(res =>{
    
if(res.status === 200)
    {

        localStorage.setItem("auth_token", res.data.token);
        localStorage.setItem("auth_userName", JSON.stringify(res.data.user));



    }else
    {
        resetLogin({...resetInput, errorlist: res.data.error });
    }
});

}









  return (

    <div>

       
<div className="yote2">
    <section>
        
        
        <div  className="content2">
            <div  className="form2">

                {/* new changes */}
                <p  style={{marginTop:"10px",marginBottom:"10px",fontSize:"32px",fontWeight:"bold" }}><i>Log in</i></p>



                <Formik
                                
                                initialValues={{
                                    email: '',
                                    password: '',
                                }}
                                validationSchema={validate}

                                
                                >
                                
                                
                                    
                                       
                            
                                        {formik => (
                                            <div>
                                            <div  className="input2">
                                            <h4>Log in using social media</h4>
                                            <ul  className=" uni2">
                                            <div id="lin2">
                                                        <a id="yo2" href="https://www.Facebook.com" target="_blank">
                                                            <li>
                                                                {/* new changes */}
                                                            <img src={face} alt="" width=" 30px" id="no2" />
                                                        <span id="sap2">Facebook</span></li> 
                                                        </a>
                                                    </div>

                                                                                        <div id="lins2">
                                                                <a id="yo2" href="https://www.Google.com" target="_blank">
                                                                    <li >
                                                    {/* new changes */}
                                                                        <img src={google} alt="" width=" 30px" id="no2" />
                                                                        <span id="sap22">Google</span></li>
                                                                </a>
                                                            </div>
                                                            <div id="linss2">
                                                                <a id="yo2" href="https://www.Linkedin.com" target="_blank">
                                            
                                                            {/* new changes */}
                                                                    <li>
                                                                    <img src={din} alt="" width=" 30px" id="no2" />
                                                                        <span id="sap2">Linkedin</span></li>  </a>
                                                            </div>
                                                            
                                                 </ul>

                                                 <img id="orr2" src={or} alt="" />

                                                 
                                            </div>
                                           
                                            <Form  onSubmit={loginSubmit}>
                                                <Formi id="insd2" onChange={handleInput} value={loginInput.email} label="" name="email" placeholder="Email Address" type="text" />
                                                <Formi id="in2" onChange={handleInput} value={loginInput.password} label="" name="password"placeholder="password"  type="password" />
                                             

                                                <div  className="input2">
                                                    <p id="ins2"  onClick={togglePopup} ><i>Forgot password?</i><IoArrowForwardCircleOutline style={{marginLeft:"10px"}}/>
                                                        <ion-icon name="chevron-forward-circle-outline" id="aro"></ion-icon></p>
                                                    </div>

                                                    <div  className="input2">
                                                    {/* new changes */}
                                                        <input id="sub2"  style={{width:"265px", fontSize:"18px",marginLeft:"12%",paddingBottom:"28px", borderRadius:"15px", marginBottom:"20px", marginTop:"2px"}} type="submit" value="Log in"/>
                                                    </div>
                                                    <div  className="input2">
                                                        <Link to={"/Register"} id="insde2" style={{textDecoration:"none", fontSize:"18px"}}><i>Dont have an account?</i> <IoArrowForwardCircleOutline style={{marginLeft:"10px"}}/></Link>
                                                    
                                                    </div>
                                           </Form>


                        

                    {popup && (

<div className="modal" id="modal" style={{width:"200px",marginLeft: "80px",marginBottom: "20px", background: "#EFFAFF", opacity:"0.92", border:"1px solid gray", boxShadow:"0px 3px 5px rgba(139, 137, 137, 0.5)" }}>
    
<p className="sms">
    <div className='popform' >
        <form action="" style={{marginBottom: "0px"}}>
        <button onClick={togglePopup} style={{marginLeft:"165PX",background:"black",color:"white",
                                      borderRadius:"50%",border:"1PX Ssolid black", width:"15px",height:"15px",fontSize:"11px"}}>
x
</button>
        
        <div className='det' style={{marginLeft:"30PX", fontSize: "10px" }}>
            <p id=' email' style={{ }}>Email address</p>
            <input type="email" name='email' onChange={handleNewInput} value={resetInput.email} id='res' style={{borderRadius: '4px', marginBottom:"10px", width:"150px"}} />
            <p id=' email'>New password</p>
            <input type="password" name='newPass' onChange={handleNewInput} value={resetInput.newPass} id='res' style={{borderRadius: '4px', marginBottom:"10px", width:"150px"}} />
            <p id=' email'>Confirm password</p>
            <input type="password" name='newConPass' onChange={handleNewInput} value={resetInput.newConPass} id='res' style={{borderRadius: '4px', marginBottom:"10px", width:"150px"}}/>
            <div  className="input2">
                    
        
            <button onClick ={takeNewInput}
                                                      className="btnpop" id="btn3" style={{marginLeft:'50px', fontSize:"12px"}}>
                                                  <i>Reset</i></button>


  {popup1 && (

<div className="modal1" id="modal1">
<p className="sms1">
    You have successfully updated your password
    <button className="close-button"  id='shut' onClick={togglePopup1}>
x
</button>
</p>

</div>


)}      


                </div>
        </div>    
        </form>
       
    </div>
    

</p>


</div>


)}




                                        </div>
                                        )}
                    </Formik>






                <form onSubmit={loginSubmit}>
                    <div  className="input2">


                        
                        


                   




                     </div>
                </form>
               
            </div>
        </div>
        <div className="imgbx">
            <img src={logo} alt="" />

            <div  className="imgbx2">
           
                <img src={invest} alt="" />
            </div>
        </div>

    </section>
</div>



    </div>

    ) 
}

export default Login;
