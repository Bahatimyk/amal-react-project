import React, { useState } from 'react';
import "./Register.css";
import logo from "../../images/logo 1.png";
import invest from "../../images/invest 6.jpg";
import or from "../../images/or4.jpg";
import {Link} from "react-router-dom";
import google from "../../images/google.png";
import { IoArrowForwardCircleOutline } from "react-icons/io5";
import din from "../../images/li.png";
import face from "../../images/fb.png";
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import {Formik,  Form, ErrorMessage, Field} from 'formik';
import * as Yup from 'yup';
import {Formi} from './Formi'; 
import 'bootstrap/dist/css/bootstrap.min.css';

import Swal from 'sweetalert2';
import Sidebar from '../../components/sidebar/Sidebar';
import Profile from '../Profile/profile';




function Register() {


    const validate =Yup.object({   
        name: Yup.string() 
        
        .required ("Username is required"),
        password: Yup.string() 
        .min(6, "Must be more than 6 characters") 
        .required ("password is required"),
        phone_number: Yup.string() 
        .min(10, "Must be more than 9 characters") 
        .required ("Phone number is required"),
        email: Yup.string() 
        .email( "Must be a valid mail") 
        .required ("Email is required"),
        password_confirmation: Yup.string() 
        .oneOf([Yup.ref('pass'), null], "password must match")
        .required ("password is required"),
    })
    






    const navigate = useNavigate();

    const [reg, setregInput] = useState({
        name:'',
        email:'',
        phone_number:'',
        password:'',
        password_confirmation:'',
        
    }); 

    const handleIput = (e) => {
        e.persist();
        setregInput({...reg, [e.target.name]: e.target.value})
    }

 const regSubmit = (e) => {
    e.preventDefault();

    const details = {
        name: reg.name,
        email: reg.email,
        phone_number: reg.phone_number,
        password: reg.password,
        password_confirmation: reg.password_confirmation,
    }
//    console.log(errors); 

    
    axios.post(`/api/register`, details ).then(res =>{
        // console.log('res', res)

        alert("register")
        if(res.status === 200)
        {
            localStorage.setItem("auth_token", res.data.token);
            localStorage.setItem("auth_userName", JSON.stringify(res.data.user));

            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'You have successfully Logged into the System',
                showConfirmButton: true,
                
              })


              navigate('/login');


         }else
        {

            
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: res.data.error,
                // footer: '<a href="">Why do I have this issue?</a>'
              })




        }

    });

}

  return (


<div>

{/* <Profile/> */}

<div className='sign'>
<div className="yote1">
    <section>
  
        <div  className="content1">
            <div  className="form1">
                {/* new changes */}
                <p style={{marginTop:"10px",marginBottom:"10px",fontSize:"26px",fontWeight:"bold" }}><i>SIGN UP</i></p>




                <Formik
                                
                                initialValues={{
                                    name:'',
                                    email: '',
                                    phone_number:'',
                                    password: '',
                                    password_confirmation:'',

                                }}
                                validationSchema={validate}
                                >
                                
                                
                                    
                                       
                            
                                        {formik => (
                                            <div>
                                            
                                            <div  className="input1">
                                            
                                            <h5><i>Create an account</i></h5>
                        <h4>Sign up using social media</h4>
                                 <ul  className=" uni1">

                                          <div id="lin2">
                                                        <a id="yo2" href="https://www.Facebook.com" target="_blank">
                                                            <li>
                                                                {/* new changes */}
                                                            <img src={face} alt="" width=" 30px" id="no2" />
                                                        <span id="sap2">Facebook</span></li> 
                                                        </a>
                                                    </div>

                                                            <div id="lins2">
                                                                <a id="yo2" href="https://www.Google.com" target="_blank">
                                                                    <li >
                                                    {/* new changes */}
                                                                        <img src={google} alt="" width=" 30px" id="no2" />
                                                                        <span id="sap22">Google</span></li>
                                                                </a>
                                                            </div>
                                                            <div id="linss2">
                                                                <a id="yo2" href="https://www.Linkedin.com" target="_blank">
                                            
                                                            {/* new changes */}
                                                                    <li>
                                                                    <img src={din} alt="" width=" 30px" id="no2" />
                                                                        <span id="sap2">Linkedin</span></li>  </a>
                                                            </div>
               
                                                    </ul>    
                                                
                                                  <img id="orr2" src={or} alt="" />
                                            
                                            </div>
                                            <Form onSubmit={regSubmit}>
                                                <Formi label="" id="insd1"  style={{width:"90%", marginLeft:"5%",  marginTop:"0px"}} name='name' onChange={handleIput} 
                                                    value={reg.name} type="text" placeholder="Username" />
                                                <Formi id="insd1" label="" style={{width:"90%", marginLeft:"5%"}} name='email' onChange={handleIput} 
                                                 value={reg.email} type="text" placeholder="Email Address" />
                                                <Formi label="" id="insd1" style={{width:"90%", marginLeft:"5%"}} name='phone_number' onChange={handleIput} 
                                                 value={reg.phone_number} type="text" placeholder="Phone number" />
                                                <Formi label="" id="in1" style={{width:"90%", marginLeft:"5%"}} name='password' onChange={handleIput} 
                                                    value={reg.password} type="password" placeholder="Password" />
                                                <Formi label="" id="in1" style={{width:"90%", marginLeft:"5%"}} name="password_confirmation" onChange={handleIput} 
                                                    value={reg.password_confirmation} type="password" placeholder="Confirm password" />
                                                <div  className="input1">
                    
                    {/* new changes */}
                        <input id="sub1" style={{width:"90%", fontSize:"18px",marginLeft:"5%", borderRadius:"15px", marginBottom:"20px", marginTop:"12px", paddingBottom:"25px"}} type="submit" value="Sign up"/>
                    </div>
                    <div  className="input1">
                        <Link to={"/login"} id="insde1" style={{textDecoration:"none"}}><i>Already have an account?</i><IoArrowForwardCircleOutline style={{marginLeft:"10px"}}/></Link>
                       
                       </div>
                                            </Form>
                                        </div>
                                        )}
                                </Formik>
            
            </div>
        </div>
        <div className="imgbx1">
            <img src={logo} alt="" />

            <div  className="imgbx3">
           
                <img src={invest} alt="" />
            </div>
        </div>

    </section>
</div>

</div>

</div>

  )
  
  
}


export default Register;
