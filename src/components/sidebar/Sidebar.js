import React from 'react';
import "./Sidebar.css"
import { slide as Menu } from 'react-burger-menu'
import {Col,Row,Container,Nav} from 'react-bootstrap';


function Sidebar() {
  function showSettings (event) {
    event.preventDefault();
}

    return (<div>
    
        {/* <Menu className="list-unstyled" >

            <nav id='sidebar'>
                <div className="sidebar-header menu-item">
                            <img src="/images/icons/172626_user_male_icon.png" className="img-fluid d-inline py-5" alt=""/>
                            <h6 className="d-inline ">Jim Garfield</h6>
                            <hr/ >
                </div>
                    <ul className="list-unstyled ">
                            <li className="pt-2 pb-2"> <a href="/dashboard" id="underline" className=""><img src="/images/icons/211676_home_icon.png" className="img-fluid me-3" height='50' width='50' alt=""/>Home</a> </li>
                            <li className=""> <a href="/profile" id="underline" className="menu-item"><img src="/images/icons/211873_person_stalker_icon.png" className="img-fluid me-3" height='50' width='50' alt=""/>Profile</a> </li>
                            <li className=""> <a href="/invest" id="underline" className="menu-item"><img src="/images/icons/Loaning.png" className="img-fluid me-3" height="50" width='50' alt=""/>Investment</a> </li>
                            <li className=""> <a href="/raise" id="underline" className="menu-item"><img src="/images/icons/Loaning.png" className="img-fluid me-3" height="50" width='50' alt=""/>Raise Capital</a> </li>
                            <li className=""> <a href="/shares" id="underline" className="menu-item"><img src="/images/icons/3890929_chart_growth_invest_market_stock_icon.png" className="img-fluid me-3" height='50' width='50' alt=""/>Shares</a> </li>             
                            <hr/>
                        </ul>
                        
                        <ul className="list-unstyled CTAs">
                            <li> <a href="/" className=""><img src="/images/icons/logo1.png" className="img-fluid  py-5 pb-1"  alt=""/></a> </li>
                        </ul>
                        </nav>
        </Menu> */}
        <Menu id=''>
            {/* <nav className='navbar'> */}

                {/* <div id='sidebar' className='navbar'>


                <div className="sidebar-header menu-item">
                            <img src="/images/icons/172626_user_male_icon.png" className=" d-inline py-1"  width={50} alt=""/>
                            <h6 className="d-inline ">Jim Garfield</h6>
                            <hr/>
                </div>
                    <ul className="list-unstyled " >
                            <li className=""> <a style={{fontSize:"small"}} href="/dashboard" id="underline" className=""><img src="/images/icons/211676_home_icon.png" className="img-fluid me-3" height={30} width={30} alt=""/>Home</a> </li>
                            <li className=""> <a href="/profile"style={{fontSize:"small"}} id="underline" className="menu-item"><img src="/images/icons/211873_person_stalker_icon.png" className="img-fluid me-3"height={30} width={30} alt=""/>Profile</a> </li>
                            <li className=""> <a href="/invest"style={{fontSize:"small"}} id="underline" className="menu-item"><img src="/images/icons/Loaning.png" className="img-fluid me-3" height={30} width={30} alt=""/>Investment</a> </li>
                            <li className=""> <a href="/raise"style={{fontSize:"small"}} id="underline" className="menu-item"><img src="/images/icons/Loaning.png" className="img-fluid me-3" height={30} width={30} alt=""/>Raise Capital</a> </li>
                            <li className=""> <a href="/shares"style={{fontSize:"small"}} id="underline" className="menu-item"><img src="/images/icons/3890929_chart_growth_invest_market_stock_icon.png" className="img-fluid me-3"height={30} width={30} alt=""/>Shares</a> </li>             
                            <hr/>
                        </ul>
                        
                        <ul className="list-unstyled CTAs">
                            <li> <a href="/" className=""><img src="/images/icons/logo1.png" class="img-fluid "  alt=""/></a> </li>
                        </ul>5                </div>
                         */}
            {/* </nav> */}
            <div id='sidebar'>

            <div className="sidebar-header menu-item">
                            <img src="/images/icons/172626_user_male_icon.png" className=" d-inline py-1 " alt=""/>
                            <h6 className="d-inline ">Jim Garfield</h6>
                </div>
                       <hr/>
                <ul>
                <li className=""> <a href="/dashboard" id="underline" className=""><img src="/images/icons/211676_home_icon.png" className="img-fluid me-3" height={40} width={40} alt=""/>Home</a> </li>
                            <li className=""> <a href="/profile" id="underline" className="menu-item"><img src="/images/icons/211873_person_stalker_icon.png" className="img-fluid me-3"height={40} width={40} alt=""/>Profile</a> </li>
                            <li className=""> <a href="/invest" id="underline" className="menu-item"><img src="/images/icons/Loaning.png" className="img-fluid me-3" height={40} width={40} alt=""/>Investment</a> </li>
                            <li className=""> <a href="/raise" id="underline" className="menu-item"><img src="/images/icons/Loaning.png" className="img-fluid me-3" height={40} width={40} alt=""/>Raise Capital</a> </li>
                            <li className=""> <a href="/shares" id="underline" className="menu-item"><img src="/images/icons/3890929_chart_growth_invest_market_stock_icon.png" className="img-fluid me-3"height={40} width={40} alt=""/>Shares</a> </li>             
                            <hr/>
                            <li> <a href="/" className=""><img src="/images/icons/logo1.png" className="img-fluid "  alt=""/></a> </li>  
                </ul>
               
            </div>
        </Menu>

        
        </div>
        )

}


export default Sidebar;
